'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersSchema extends Schema {
  up () {
    this.create('orders', (table) => {
      table.increments()
      table.date('order_date').notNullable()
      table.decimal('total_amount', 20, 2).notNullable()
      table.integer('clients_id')
      .unsigned()
      .references('clients.id')
      .notNullable()
      .onUpdate('NO ACTION')
      .onDelete('NO ACTION')
      table.timestamps()
    })
  }

  down () {
    this.drop('orders')
  }
}

module.exports = OrdersSchema
