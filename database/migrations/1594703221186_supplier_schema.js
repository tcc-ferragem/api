'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SupplierSchema extends Schema {
  up () {
    this.create('suppliers', (table) => {
      table.increments()
      table.string('firm_name', 50).notNullable()
      table.string('comercial_name', 50)
      table.string('cnpj', 15)
      table.string('address', 100)
      table.string('city', 45)
      table.string('state', 2)
      table.boolean('is_susidiary').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('suppliers')
  }
}

module.exports = SupplierSchema
