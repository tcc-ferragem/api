'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrdersHasProductsSchema extends Schema {
  up () {
    this.create('orders_has_products', (table) => {
      table.integer('orders_id')
      .unsigned()
      .references('orders.id')
      .notNullable()
      table.integer('products_id')
      .unsigned()
      .references('products.id')
      .notNullable()
    })
  }

  down () {
    this.drop('orders_has_products')
  }
}

module.exports = OrdersHasProductsSchema
